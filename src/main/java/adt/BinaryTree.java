package adt;

/**
 * Datenstruktur: Binärer Baum
 *
 * Wir gehen davon aus, dass der binäre Baum bei jedem Einfügen eines neuen
 * Elements sortiert wird.
 *
 * @param <T> zu speichernde Klasse
 */
public class BinaryTree<T extends Comparable<T>> implements Tree<T> {

    /**
     * Wurzel unseres binären Baumes.
     */
    private Node<T> root = null;

    /*
     * Die folgenden Methoden verwenden das Reflection API von Java, mit welchem
     * wir die Methode visit() dynamisch gestalten können. Für die Funktionalität
     * des binären Baume ist dies nicht zwingend notwendig. Aus diesem Grund
     * sind die folgenden Codefragmente auskommentiert.
     *
     * private static Method visit; // Zu verwendende Methode bei der Traversierung
     * private static final Object[] visitArgs = new Object[1]; // ..Parameter zur obigen methode
     *
     * private static final Class[] paramType = {Node.class}; // Parametertype für visit()
     *
     * static Method theAdd1;       // Zählen der besuchten Knoten
     * static Method outputMethod;  // Ausgabe nach dem Besuchen eines Knotens
     *
     * static {
     *     try {
     *         Class<BinaryTree> treeClass = BinaryTree.class;
     *         theAdd1 = treeClass.getMethod("add1", paramType);
     *         outputMethod = treeClass.getMethod("output", paramType);
     *     } catch (Exception ignore) {}
     * }
     */

    /**
     * Zähler für die Anzahl vorhandener Knoten
     */
    private int count;

    /**
     * Besuchen eines Knotens und ausführen einer bestimmten Methode. In unserem
     * Fall geben wir den Inhalt des Knotens aus und zählen gleichzeitig die
     * Anzahl besuchter Knoten.
     *
     * @param node besuchter Knoten
     */
    public void visit(Node<T> node) {
        System.out.print(node.getPayload() + " ");
        count++;
    }

    /**
     * @return Liefert den Wert true, falls der Baum leer ist
     */
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * Liefert die Wurzel des Baumes.
     *
     * @return Liefert den Wert null, falls der Baum leer ist
     */
    public Node<T> getRoot() {
        return (isEmpty()) ? null : root;
    }

    /**
     * Einfügend eines neuen Elementes in die Baumstruktur. Damit das Einfügen funktioniert
     * müssen die Eigenschaften eines BST (Binary Search Tree) erfüllt sein. Dies bedeutet,
     * dass der Baum zu jeder gegebenen Zeit sortiert vorliegt.
     *
     * @param element Einzufügendes Element
     * @return liefert den Wert true, wenn das Element eingefügt werden konnte.
     */
    @Override
    public boolean insert(T element) {

        Node<T> parent, current = root;

        do {
            int cmp = current.getPayload().compareTo(element);
            if (cmp == 0)
                return false;
            else {
                parent = current;
                current = (cmp > 0 ? current.getLeft() : current.getRight());
            }

        } while (current != null);

        Node<T> node = new Node<>(element, null, null);

        if (parent.getPayload().compareTo(element) > 0)
            parent.setLeft(node);
        else
            parent.setRight(node);

        return true;
    }


    /**
     * Erstellt eine Baumstruktur mit den angegebenen Parameter.
     *
     * <b>Vorsicht:</b> Der rechte und linke Teilbaum wird nicht geklont!
     *
     * @param payload Wurzel der Baumstruktur
     * @param left    Linker Teilbaum
     * @param right   Rechter Teilbaum
     */
    public void makeTree(T payload, Node<T> left, Node<T> right) {
        this.root = new Node<>(payload, left, right);
    }

    /**
     * Löscht den linken Teilbaum.
     *
     * @return Gelöschter Teilbaum
     * @throws IllegalArgumentException wenn der Teilbaum leer ist
     */
    public Tree<T> removeLeftSubtree() {
        if (isEmpty())
            throw new IllegalArgumentException("tree is empty");

        // Linken Teilbaum trennen
        BinaryTree<T> leftSubtree = new BinaryTree<>();
        leftSubtree.root = root.getLeft();
        root.setLeft(null);

        return leftSubtree;
    }

    /**
     * Löscht den rechten Teilbaum.
     *
     * @return Gelöschter Teilbaum
     * @throws IllegalArgumentException wenn der Teilbaum leer ist
     */
    public Tree<T> removeRightSubtree() {
        if (isEmpty())
            throw new IllegalArgumentException("tree is empty");

        // Rechten Teilbaum trennen
        BinaryTree<T> rightSubtree = new BinaryTree<>();
        rightSubtree.root = root.getRight();
        root.setRight(null);

        return rightSubtree;
    }

    /**
     * Preorder Traversierung.
     */
    public void preOrder() {
        traversePreOrder(root);
    }

    /**
     * Preorder Traversierung.
     *
     * @param node Knoten des aktuellen Teilbaums
     */
    private void traversePreOrder(Node<T> node) {
        if (node != null) {
            visit(node);
            traversePreOrder(node.getLeft());
            traversePreOrder(node.getRight());
        }
    }

    /**
     * Inorder Traversierung.
     */
    public void inOrder() {
        traverseInOrder(root);
    }

    /**
     * Inorder Traversierung.
     *
     * @param node Knoten des aktuellen Teilbaums
     */
    private void traverseInOrder(Node<T> node) {
        if (node != null) {
            traverseInOrder(node.getLeft());
            visit(node);
            traverseInOrder(node.getRight());
        }
    }

    /**
     * Postorder Traversierung.
     */
    public void postOrder() {
        traversePostOrder(root);
    }

    /**
     * Postorder Traversierung.
     *
     * @param node Knoten des aktuellen Teilbaums
     */
    private void traversePostOrder(Node<T> node) {
        if (node != null) {
            traversePostOrder(node.getLeft());
            traversePostOrder(node.getRight());
            visit(node);
        }
    }

    /**
     * Levelorder Traversierung.
     */
    public void levelOrder() {
        ArrayQueue<Node<T>> queue = new ArrayQueue<>();

        Node<T> node = root;
        while (node != null) {
            visit(node);

            // put t's children on queue
            if (node.getLeft() != null)
                queue.put(node.getLeft());
            if (node.getRight() != null)
                queue.put(node.getRight());

            // get next node to visit
            node = queue.remove();
        }
    }

    /**
     * Liefert die Anzahl der Knoten im Baum.
     *
     * @return Anzahl Knoten im Baum
     */
    public int size() {
        count = 0;
        inOrder();
        return count;
    }

    /**
     * Liefert die Tiefe des Baumes beginnend mit dem Startknoten.
     *
     * @return Tiefe des Baumes
     */
    public int height() {
        return countHeight(root);
    }


    /**
     * Liefert die Tiefe des Baumes beginnend mit dem angegebenen Knoten.
     *
     * @param treeNode Von diesem Knoten soll die Tiefe berechnet werden
     * @return Tiefe des Baumes
     */
    private int countHeight(Node<T> treeNode) {
        if (treeNode == null)
            return 0;

        int leftHeight = countHeight(treeNode.getLeft());
        int rightHeight = countHeight(treeNode.getRight());

        return (leftHeight > rightHeight) ? ++leftHeight : ++rightHeight;
    }

}
