package adt;

/**
 * Definition aller Methoden, welche durch eine beliebige Listenimplementation
 * zur Verfügung gestellt werden müssen. Hier machen wir noch keine Aussage
 * darüber, ob die einzelnen Elemente der Liste in sortierter Reihenfolge vorliegen
 * oder nicht.
 *
 * @param <T> Datentyp, den wir in der Liste verwalten
 */
public interface List<T extends  Comparable<T>> {
   /**
    * Liefert den Wert true, wenn die Datenstruktur leer ist.
    *
    * @return true, wenn die Datenstruktur leer ist.
    */
   boolean isEmpty();

   /**
    * Liefert das erste Element der Datenstruktur.
    *
    * @return null, wenn die Struktur leer ist.
    */
   T getFirst();

   /**
    * Liefert das letzte Element der Datenstruktur.
    *
    * @return null, wenn die Struktur leer ist.
    */
   T getLast();

   /**
    * Liefert die Anzahl der gespeicherten Elemente.
    *
    * @return Anzahl gespeicherten Elemente.
    */
   int size();

   /**
    * Fügt ein neues Element in der Datenstruktur ein.
    *
    * @param element Hinzuzufügendes Element
    * @return null, wenn das Element nicht hinzugefügt werden konnte.
    */
   boolean insert(T element);

}
