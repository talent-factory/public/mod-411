package adt;

import org.jetbrains.annotations.NotNull;

/**
 * Diese Klasse speichert die einzelnen Elemente in einer verketteten
 * Liste, wobei die einzelnen Elemente in aufsteigender Reihenfolge sortiert
 * sind.
 *
 * @param <T> Datentyp des zu speichernden Elementes
 */
public class LinkedList<T extends Comparable<T>> implements List<T> {

    // Startknoten
    private Node<T> root = null;

    /**
     * Überprüft, ob sich bereits elemente in der Liste befinden.
     *
     * @return liefert den Wert true, falls die Liste leer ist
     */
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * Liefert das erste Element der Datenstruktur.
     *
     * @return null, wenn die Struktur leer ist.
     */
    @Override
    public T getFirst() {
        return (root == null) ? null : root.getPayload();
    }

    /**
     * Liefert das letzte Element der Datenstruktur.
     *
     * @return null, wenn die Struktur leer ist.
     */
    @Override
    public T getLast() {
        Node<T> current = root, previous = root;
        while (current != null) {
            previous = current;
            current = current.getRight();
        }
        return (previous == null) ? null : previous.getPayload();
    }

    /**
     * Liefert die Anzahl der gespeicherten Elemente.
     *
     * @return Anzahl gespeicherter Elemente
     */
    @Override
    public int size() {
        int size = 0;

        Node<T> current = root;
        while (current != null) {
            current = current.getRight();
            size++;
        }

        return size;
    }

    /**
     * Einfügend eines neuen Elementes in den Array. Damit das Einfügen funktioniert
     * müssen die Eigenschaften eines sortierten Arrays erfüllt sein.
     *
     * @param element Einzufügendes Element
     * @return liefert den Wert true, wenn das Element eingefügt werden konnte.
     */
    public boolean insert(@NotNull T element) {

        if (isEmpty()) {
            root = new Node<>(element);
            return true;
        }

        Node<T> parent, current = root;
        do {
            int cmp = current.getPayload().compareTo(element);
            if (cmp == 0)
                return false;
            else {
                parent = current;
                current = (cmp > 0) ? null : current.getRight();
            }
        } while (current != null);

        Node<T> node = new Node<>(element);
        if (parent.getPayload().compareTo(element) > 0) {
            node.setRight(parent);
            if (root == node.getRight())
                root = node;
            node.setLeft(parent.getLeft());
            parent.setLeft(node);
        } else {
            node.setRight(parent.getRight());
            node.setLeft(parent);
            parent.setRight(node);
        }

        return true;
    }
}
