package adt;

import java.lang.reflect.Method;

public interface Tree<T extends Comparable<T>> {

    /**
     * @return liefert den Wert true, falls der Baum leer ist
     */
    boolean isEmpty();

    /**
     * Liefert die Wurzel des Baumes.
     *
     * @return Liefert den Wert null, falls der Baum leer ist
     */
    Node<T> getRoot();

    /**
     * Einfügend eines neuen Elementes in die Baumstruktur. Damit das Einfügen funktioniert
     * müssen die Eigenschaften eines BST (Binary Search Tree) erfüllt sein. Dies bedeutet,
     * dass der Baum zu jeder gegebenen Zeit sortiert vorliegt.
     *
     * @param element Einzufügendes Element
     * @return liefert den Wert true, wenn das Element eingefügt werden konnte.
     */
    boolean insert(T element);

    /**
     * Erstellt eine Baumstruktur mit den angegebenen Parameter.
     *
     * <b>Vorsicht:</b> Der rechte und linke Teilbaum wird nicht geklont!
     *
     * @param payload Wurzel der Baumstruktur
     * @param left    Linker Teilbaum
     * @param right   Rechter Teilbaum
     */
     void makeTree(T payload, Node<T> left, Node<T> right);

    /**
     * Löscht den linken Teilbaum.
     *
     * @return Gelöschter Teilbaum
     */
    Tree<T> removeLeftSubtree();

    /**
     * Löscht den rechten Teilbaum.
     *
     * @return Gelöschter Teilbaum
     */
    Tree<T> removeRightSubtree();

    void preOrder();

    void inOrder();

    void postOrder();

    void levelOrder();

}
