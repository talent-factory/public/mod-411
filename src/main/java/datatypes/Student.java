package datatypes;

import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
public class Student implements Comparable<Student> {

    private static int counter = 1;
    private int id;
    private String name;

    public Student(String name) {
        this.id = counter++;
        this.name = name;
    }

    /**
     * Sortieren der Elemente nach der Eigenschaft "name".
     *
     * @param other zu vergleichender Student
     * @return -1, 0 oder +1, falls der Name kleiner, gleich oder grösser ist als
     * der Student in der Parameterübergabe
     */
    public int compareTo(@NotNull Student other) {
        return name.compareTo(other.getName());
    }
}
