package datatypes;

import lombok.Data;

import java.awt.*;

@Data
public class Shirt {

    private final Color color;
    private final Size size;

    public enum Size {
        L, M, S, XL
    }

    public Shirt(Color color, Size size) {
       this.color = color;
       this.size = size;
    }

}
