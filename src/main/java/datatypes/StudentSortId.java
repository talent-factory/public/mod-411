package datatypes;

import java.util.Comparator;

public class StudentSortId implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        int result = 0;
        if (o1.getId() > o2.getId()) {
            result = 1;
        } else if (o1.getId() < o2.getId()) {
            result = -1;
        }
        return result;
    }
}
