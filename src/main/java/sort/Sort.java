package sort;

/**
 * Wenn verschiedene Algorithmen für die Lösung desselben Problems entwickelt
 * werden, ist es zweckmässig, das Entwurfsmuster <b>Strategy</b> zu verwenden.
 * In diesem Fall besteht es daraus, dass die Signatur der Sortiermethoden in
 * einer Schnittstelle vorgegeben ist:
 *
 * @param <E> der zu sortierender Datentyp
 */
public interface Sort<E extends Comparable<E>> {

    /**
     * Sortieren der Daten nach einem bestimmten Algorithmus.
     *
     * @param collection zu sortierende Daten
     */
    void sort(E[] collection);

}
