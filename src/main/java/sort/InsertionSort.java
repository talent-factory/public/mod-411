package sort;

public class InsertionSort<E extends Comparable<E>> implements Sort<E> {

    @Override
    public void sort(E[] collection) {
        for (int i = 1; i < collection.length; i++) {
            E value = collection[i];
            int j = i;
            while (j > 0 && collection[j - 1].compareTo(value) > 0) {
                collection[j] = collection[j - 1];
                j = j - 1;
            }
            collection[j] = value;
        }
    }
}
