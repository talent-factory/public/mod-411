package sort;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InsertionSortTest {

    private final InsertionSort<Integer> integerSort = new InsertionSort<>();

    @Test
    void sort() {
        Integer[] list = {0, 9, 4, 6, 2, 8, 5, 1, 7, 3};
        Integer[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        integerSort.sort(list);
        assertArrayEquals(expected, list);
    }

}
