package adt;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ArrayListTest {

    private ArrayList<Integer> arrayList;

    @BeforeEach
    public void setUp() {
        arrayList = new ArrayList<>();
    }

    @Test
    public void add() {
        arrayList.add(10);
        arrayList.add(20);

        assertThat(arrayList.size(), is(2));
        assertThat(arrayList.get(1), is(20));
    }
}
