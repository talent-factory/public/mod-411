package adt;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Überprüfen der Klasse LinkedArrayList.
 */
class LinkedListTest {

    private LinkedList<Integer> list;

    @BeforeEach
    void setUp() {
        list = new LinkedList<>();
    }

    /**
     * Ist die leere Liste auch wirklich leer?
     */
    @Test
    void isEmpty() {
        assertTrue(list.isEmpty());
        assertSame(list.getFirst(), list.getLast());
    }

    /**
     * Mit diesen Tests stellen wir sicher, dass die Anzahl der eingefügten
     * Elemente übereinstimmt.
     */
    @Test
    void size() {
        assertEquals(list.size(), 0);

        list.insert(5);
        assertEquals(list.size(), 1);
    }

    /**
     * Mit der Funktion getFirst() lesen wir das erste Element, welches eingefügt
     * wurde.
     */
    @Test
    void getFirst() {
        assertNull(list.getFirst());

        list.insert(5);
        assertEquals(5, list.getFirst());

        list.insert(7);
        assertEquals(5, list.getFirst());
    }

    /**
     * Mit der Funktion getLast() lesen wir das letzte Element, welches eingefügt
     * wurde.
     */
    @Test
    void getLast() {
        assertNull(list.getLast());

        list.insert(6);
        assertEquals(6, list.getLast());

        list.insert(5);
        assertEquals(6, list.getLast());

        list.insert(7);
        assertEquals(7, list.getLast());
    }

    /**
     * Einfügen und überprüfen eines einzelnen Elementes, welches in der
     * Datenstruktur eingefügt wird.
     */
    @Test
    @DisplayName("insert(5)")
    void insert() {
        assertTrue(list.insert(5));
        assertEquals(5, list.getFirst());
    }

    @Test
    @DisplayName("insert([5,6,7])")
    void insert567() {
        assertTrue(list.insert(5));
        assertTrue(list.insert(6));
        assertTrue(list.insert(7));

        assertEquals(5, list.getFirst());
        assertEquals(7, list.getLast());
    }

    @Test
    @DisplayName("insert([6,7,5])")
    void insert675() {
        assertTrue(list.insert(6));
        assertTrue(list.insert(7));
        assertTrue(list.insert(5));

        assertEquals(5, list.getFirst());
        assertEquals(7, list.getLast());
    }

    @Test
    @DisplayName("insert([7,5,6])")
    void insert756() {
        assertTrue(list.insert(7));
        assertTrue(list.insert(5));
        assertTrue(list.insert(6));

        assertEquals(5, list.getFirst());
        assertEquals(7, list.getLast());
    }

    @Test
    @DisplayName("insert([6,5,7])")
    void insert657() {
        assertTrue(list.insert(6));
        assertTrue(list.insert(5));
        assertTrue(list.insert(7));

        assertEquals(5, list.getFirst());
        assertEquals(7, list.getLast());
    }

    @Test
    @DisplayName("insert([5,7,6])")
    void insert576() {
        assertTrue(list.insert(5));
        assertTrue(list.insert(7));
        assertTrue(list.insert(6));

        assertEquals(5, list.getFirst());
        assertEquals(7, list.getLast());
    }

    @Test
    @DisplayName("insert([7,6,5])")
    void insert765() {
        assertTrue(list.insert(7));
        assertTrue(list.insert(6));
        assertTrue(list.insert(5));

        assertEquals(5, list.getFirst());
        assertEquals(7, list.getLast());
    }
}
