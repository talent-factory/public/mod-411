package datatypes;

import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;

public class ShirtTest {

    @Test
    public void add() {
        List<Shirt> list = new ArrayList<>();

        list.add(new Shirt(Color.BLUE, Shirt.Size.S));
        list.add(new Shirt(Color.GRAY, Shirt.Size.M));
        list.add(new Shirt(Color.RED, Shirt.Size.L));
        list.add(new Shirt(Color.CYAN, Shirt.Size.XL));

        for (Shirt shirt : list) {
            assertThat(shirt, hasProperty("color"));
        }
    }
}
