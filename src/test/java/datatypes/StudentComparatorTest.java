package datatypes;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class StudentComparatorTest {

    @Test
    public void comparatorTest() {
        List<Student> students = new ArrayList<>();

        students.add(new Student("Peter Müller"));
        students.add(new Student("Alex Peters"));
        students.add(new Student("Dan Ramsey"));

        students.sort(new StudentSortName());
        assertThat(students.remove(0).getId(), is(2));

        students.sort(new StudentSortId());
        assertThat(students.remove(0).getId(), is(1));
    }
}
