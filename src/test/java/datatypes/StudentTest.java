package datatypes;

import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.TreeSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class StudentTest {

    @Test
    void sort() {

        // Sortierte Menge von Studenten
        Set<Student> students = new TreeSet<>();

        students.add(new Student("Peter Müller"));
        students.add(new Student("Alex Peters"));
        students.add(new Student("Dan Ramsey"));

        // Liest das erste Element aus der sortierten Menge
        Student student = students.iterator().next();
        assertThat(student.getName(), is("Alex Peters"));


    }
}
